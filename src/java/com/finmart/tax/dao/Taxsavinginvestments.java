package com.finmart.tax.dao;
// Generated Jun 4, 2015 6:09:07 PM by Hibernate Tools 3.6.0



/**
 * Taxsavinginvestments generated by hbm2java
 */
public class Taxsavinginvestments  implements java.io.Serializable {


     private Integer taxSavingsInvestmentsId;
     private Investmenttoolstypes investmenttoolstypes;
     private Useritmapping useritmapping;
     private int investmentAmount;

    public Taxsavinginvestments() {
    }

    public Taxsavinginvestments(Investmenttoolstypes investmenttoolstypes, Useritmapping useritmapping, int investmentAmount) {
       this.investmenttoolstypes = investmenttoolstypes;
       this.useritmapping = useritmapping;
       this.investmentAmount = investmentAmount;
    }
   
    public Integer getTaxSavingsInvestmentsId() {
        return this.taxSavingsInvestmentsId;
    }
    
    public void setTaxSavingsInvestmentsId(Integer taxSavingsInvestmentsId) {
        this.taxSavingsInvestmentsId = taxSavingsInvestmentsId;
    }
    public Investmenttoolstypes getInvestmenttoolstypes() {
        return this.investmenttoolstypes;
    }
    
    public void setInvestmenttoolstypes(Investmenttoolstypes investmenttoolstypes) {
        this.investmenttoolstypes = investmenttoolstypes;
    }
    public Useritmapping getUseritmapping() {
        return this.useritmapping;
    }
    
    public void setUseritmapping(Useritmapping useritmapping) {
        this.useritmapping = useritmapping;
    }
    public int getInvestmentAmount() {
        return this.investmentAmount;
    }
    
    public void setInvestmentAmount(int investmentAmount) {
        this.investmentAmount = investmentAmount;
    }




}


