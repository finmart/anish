package com.finmart.tax.dao;
// Generated Jun 4, 2015 6:09:07 PM by Hibernate Tools 3.6.0


import java.util.Date;

/**
 * PaymentMaster generated by hbm2java
 */
public class PaymentMaster  implements java.io.Serializable {


     private Integer payId;
     private ServiceMaster serviceMaster;
     private EnqMaster enqMaster;
     private PaymetmodeMaster paymetmodeMaster;
     private String paymentStatus;
     private int amtPayable;
     private int govtFees;
     private int courierCharges;
     private float servicetax;
     private int amount;
     private Date payDate;
     private String comment;

    public PaymentMaster() {
    }

    public PaymentMaster(ServiceMaster serviceMaster, EnqMaster enqMaster, PaymetmodeMaster paymetmodeMaster, String paymentStatus, int amtPayable, int govtFees, int courierCharges, float servicetax, int amount, Date payDate, String comment) {
       this.serviceMaster = serviceMaster;
       this.enqMaster = enqMaster;
       this.paymetmodeMaster = paymetmodeMaster;
       this.paymentStatus = paymentStatus;
       this.amtPayable = amtPayable;
       this.govtFees = govtFees;
       this.courierCharges = courierCharges;
       this.servicetax = servicetax;
       this.amount = amount;
       this.payDate = payDate;
       this.comment = comment;
    }
   
    public Integer getPayId() {
        return this.payId;
    }
    
    public void setPayId(Integer payId) {
        this.payId = payId;
    }
    public ServiceMaster getServiceMaster() {
        return this.serviceMaster;
    }
    
    public void setServiceMaster(ServiceMaster serviceMaster) {
        this.serviceMaster = serviceMaster;
    }
    public EnqMaster getEnqMaster() {
        return this.enqMaster;
    }
    
    public void setEnqMaster(EnqMaster enqMaster) {
        this.enqMaster = enqMaster;
    }
    public PaymetmodeMaster getPaymetmodeMaster() {
        return this.paymetmodeMaster;
    }
    
    public void setPaymetmodeMaster(PaymetmodeMaster paymetmodeMaster) {
        this.paymetmodeMaster = paymetmodeMaster;
    }
    public String getPaymentStatus() {
        return this.paymentStatus;
    }
    
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
    public int getAmtPayable() {
        return this.amtPayable;
    }
    
    public void setAmtPayable(int amtPayable) {
        this.amtPayable = amtPayable;
    }
    public int getGovtFees() {
        return this.govtFees;
    }
    
    public void setGovtFees(int govtFees) {
        this.govtFees = govtFees;
    }
    public int getCourierCharges() {
        return this.courierCharges;
    }
    
    public void setCourierCharges(int courierCharges) {
        this.courierCharges = courierCharges;
    }
    public float getServicetax() {
        return this.servicetax;
    }
    
    public void setServicetax(float servicetax) {
        this.servicetax = servicetax;
    }
    public int getAmount() {
        return this.amount;
    }
    
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public Date getPayDate() {
        return this.payDate;
    }
    
    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }
    public String getComment() {
        return this.comment;
    }
    
    public void setComment(String comment) {
        this.comment = comment;
    }




}


