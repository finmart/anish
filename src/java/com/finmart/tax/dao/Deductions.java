package com.finmart.tax.dao;
// Generated Jun 4, 2015 6:09:07 PM by Hibernate Tools 3.6.0



/**
 * Deductions generated by hbm2java
 */
public class Deductions  implements java.io.Serializable {


     private Integer deductionId;
     private Deductiontypes deductiontypes;

    public Deductions() {
    }

    public Deductions(Deductiontypes deductiontypes) {
       this.deductiontypes = deductiontypes;
    }
   
    public Integer getDeductionId() {
        return this.deductionId;
    }
    
    public void setDeductionId(Integer deductionId) {
        this.deductionId = deductionId;
    }
    public Deductiontypes getDeductiontypes() {
        return this.deductiontypes;
    }
    
    public void setDeductiontypes(Deductiontypes deductiontypes) {
        this.deductiontypes = deductiontypes;
    }




}


