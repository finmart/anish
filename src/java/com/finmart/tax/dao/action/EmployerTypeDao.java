/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.dao.action;

import com.finmart.tax.dao.Employertype;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author admin
 */
public class EmployerTypeDao implements DaoAction<Employertype>{
    
    Session session;

    public EmployerTypeDao(Session session) {
        this.session = session;
    }
    
    @Override
    public void save(Employertype e) {
        session.save(e);
        session.close();
      
    }

    @Override
    public void update(Employertype e) {
        
    }

    @Override
    public void delete(Employertype e) {
        
    }

    @Override
    public Employertype get(Employertype e) {
        return null;
        
    }

    @Override
    public List<Employertype> getAll() {
        return null;
        
    }

    @Override
    public void beginTransaction() {
        session.beginTransaction();
    }

    @Override
    public void commit() {
        session.getTransaction().commit();
    }

  
}
