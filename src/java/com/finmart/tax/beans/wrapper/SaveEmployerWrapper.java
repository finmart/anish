/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author admin
 */
public class SaveEmployerWrapper {

    
    
    private String EmployerName;
    private String EmployerType;
    private String EmployerAddress;
    private String EmploymentPeriod;
    private String EmployerCity;

    public SaveEmployerWrapper() {
    }

    public SaveEmployerWrapper(String EmployerName, String EmployerType, String EmployerAddress, String EmployerPeriod, String EmployerCity) {
        this.EmployerName = EmployerName;
        this.EmployerType = EmployerType;
        this.EmployerAddress = EmployerAddress;
        this.EmploymentPeriod = EmployerPeriod;
        this.EmployerCity = EmployerCity;
    }

    /**
     * @return the EmployerName
     */
    public String getEmployerName() {
        return EmployerName;
    }

    /**
     * @param EmployerName the EmployerName to set
     */
    public void setEmployerName(String EmployerName) {
        this.EmployerName = EmployerName;
    }

    /**
     * @return the EmployerType
     */
    public String getEmployerType() {
        return EmployerType;
    }

    /**
     * @param EmployerType the EmployerType to set
     */
    public void setEmployerType(String EmployerType) {
        this.EmployerType = EmployerType;
    }

    /**
     * @return the EmployerAddress
     */
    public String getEmployerAddress() {
        return EmployerAddress;
    }

    /**
     * @param EmployerAddress the EmployerAddress to set
     */
    public void setEmployerAddress(String EmployerAddress) {
        this.EmployerAddress = EmployerAddress;
    }

    /**
     * @return the EmployerPeriod
     */
    public String getEmployerPeriod() {
        return EmploymentPeriod;
    }

    /**
     * @param EmployerPeriod the EmployerPeriod to set
     */
    public void setEmployerPeriod(String EmployerPeriod) {
        this.EmploymentPeriod = EmployerPeriod;
    }
    
    // set employer city
    public void setEmployerCity(String EmployerCity) {
        this.EmployerCity = EmployerCity;
    }

    
    //get Employer city
    public String getEmployerCity() {
        return EmployerCity;
    }        
    
}
