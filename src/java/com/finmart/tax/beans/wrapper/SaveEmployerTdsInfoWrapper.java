/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author admin
 */
public class SaveEmployerTdsInfoWrapper {

    
    
    private String TANno;
    private String TaxDeductAmount;
    private String NameOfDeductor;
    private String AddressOfDeductor;
    
    
    public SaveEmployerTdsInfoWrapper(){
        
    }
    
    
    
    public SaveEmployerTdsInfoWrapper(String TANno, String TaxDeductAmount, String NameOfDeductor, String AddressOfDeductor) {
        this.TANno = TANno;
        this.TaxDeductAmount = TaxDeductAmount;
        this.NameOfDeductor = NameOfDeductor;
        this.AddressOfDeductor = AddressOfDeductor;
    }
    
    
    public void setTANno(String TANno) {
        this.TANno = TANno;
    }

    public void setTaxDeductAmount(String TaxDeductAmount) {
        this.TaxDeductAmount = TaxDeductAmount;
    }

    public void setNameOfDeductor(String NameOfDeductor) {
        this.NameOfDeductor = NameOfDeductor;
    }

    public void setAddressOfDeductor(String AddressOfDeductor) {
        this.AddressOfDeductor = AddressOfDeductor;
    }

    public String getTANno() {
        return TANno;
    }

    public String getTaxDeductAmount() {
        return TaxDeductAmount;
    }

    public String getNameOfDeductor() {
        return NameOfDeductor;
    }

    public String getAddressOfDeductor() {
        return AddressOfDeductor;
    }

}
