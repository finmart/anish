/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author admin
 */
public class SaveAllowancesWrapper {

    
    
    private String Description;
    private String AllowanceType;
    private String Value;
    private String SalaryIncome;
   

    public SaveAllowancesWrapper() {
    }
   
    
     public SaveAllowancesWrapper(String Description, String AllowanceType, String Value, String SalaryIncome) {
        this.Description = Description;
        this.AllowanceType = AllowanceType;
        this.Value = Value;
        this.SalaryIncome = SalaryIncome;
    }
    
    
    
    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setAllowanceType(String AllowanceType) {
        this.AllowanceType = AllowanceType;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }

    public void setSalaryIncome(String SalaryIncome) {
        this.SalaryIncome = SalaryIncome;
    }

    public String getDescription() {
        return Description;
    }

    public String getAllowanceType() {
        return AllowanceType;
    }

    public String getValue() {
        return Value;
    }

    public String getSalaryIncome() {
        return SalaryIncome;
    }

    
}
