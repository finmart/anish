/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

/**
 *
 * @author admin
 */
public class SaveEmployerTypeWrapper {

    
    private String EmployerType;
    
    public SaveEmployerTypeWrapper(){
        
    }
    
    public SaveEmployerTypeWrapper(String EmployerType) {
        this.EmployerType = EmployerType;
    }
    
    
    public void setEmployerType(String EmployerType) {
        this.EmployerType = EmployerType;
    }

    public String getEmployerType() {
        return EmployerType;
    }
    
    
}
