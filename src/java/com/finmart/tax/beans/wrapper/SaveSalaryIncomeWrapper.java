/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

import com.finmart.tax.beans.wrapper.SaveEmployerWrapper;
import com.finmart.tax.beans.wrapper.SaveEmployerTypeWrapper;
import com.finmart.tax.beans.wrapper.SaveAllowancesWrapper;
import com.finmart.tax.beans.wrapper.SaveEmployerTdsInfoWrapper;
import com.finmart.tax.beans.wrapper.SaveVoluntaryTaxPaymentWrapper;
/**
 *
 * @author admin
 */
public class SaveSalaryIncomeWrapper {

    public void setEmployerWrapper(SaveEmployerWrapper employerWrapper) {
        this.employerWrapper = employerWrapper;
    }

    public void setEmployerTypeWrapper(SaveEmployerTypeWrapper employerTypeWrapper) {
        this.employerTypeWrapper = employerTypeWrapper;
    }

    public void setEmployerAllowancesWrapper(SaveAllowancesWrapper employerAllowancesWrapper) {
        this.employerAllowancesWrapper = employerAllowancesWrapper;
    }

    public void setEmployerTdsInfoWrapper(SaveEmployerTdsInfoWrapper employerTdsInfoWrapper) {
        this.employerTdsInfoWrapper = employerTdsInfoWrapper;
    }

    public void setEmployerVoluntaryTaxPaymentWrapper(SaveVoluntaryTaxPaymentWrapper employerVoluntaryTaxPaymentWrapper) {
        this.employerVoluntaryTaxPaymentWrapper = employerVoluntaryTaxPaymentWrapper;
    }

    public SaveEmployerWrapper getEmployerWrapper() {
        return employerWrapper;
    }

    public SaveEmployerTypeWrapper getEmployerTypeWrapper() {
        return employerTypeWrapper;
    }

    public SaveAllowancesWrapper getEmployerAllowancesWrapper() {
        return employerAllowancesWrapper;
    }

    public SaveEmployerTdsInfoWrapper getEmployerTdsInfoWrapper() {
        return employerTdsInfoWrapper;
    }

    public SaveVoluntaryTaxPaymentWrapper getEmployerVoluntaryTaxPaymentWrapper() {
        return employerVoluntaryTaxPaymentWrapper;
    }

    public SaveSalaryIncomeWrapper(SaveEmployerWrapper employerWrapper, SaveEmployerTypeWrapper employerTypeWrapper, SaveAllowancesWrapper employerAllowancesWrapper, SaveEmployerTdsInfoWrapper employerTdsInfoWrapper, SaveVoluntaryTaxPaymentWrapper employerVoluntaryTaxPaymentWrapper) {
        this.employerWrapper = employerWrapper;
        this.employerTypeWrapper = employerTypeWrapper;
        this.employerAllowancesWrapper = employerAllowancesWrapper;
        this.employerTdsInfoWrapper = employerTdsInfoWrapper;
        this.employerVoluntaryTaxPaymentWrapper = employerVoluntaryTaxPaymentWrapper;
    }

    public SaveSalaryIncomeWrapper() {
    }
    
    SaveEmployerWrapper employerWrapper;
    SaveEmployerTypeWrapper employerTypeWrapper;
    SaveAllowancesWrapper employerAllowancesWrapper;
    SaveEmployerTdsInfoWrapper employerTdsInfoWrapper;
    SaveVoluntaryTaxPaymentWrapper employerVoluntaryTaxPaymentWrapper;
    
}
