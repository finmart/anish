/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.finmart.tax.beans.wrapper;

import java.util.Date;

/**
 *
 * @author admin
 */
public class SaveVoluntaryTaxPaymentWrapper {

   
    

    
    private String BankName;
    private String BSRName;
    private String ChallanNumber;
    private Date DateOfDeposit;
    private String AmountOfVoluntaryTaxPayment;
    
    public SaveVoluntaryTaxPaymentWrapper() {
    }
    
    
    public SaveVoluntaryTaxPaymentWrapper(String BankName, String BSRName, String ChallanNumber, Date DateOfDeposit, String AmountOfVoluntaryTaxPayment) {
        this.BankName = BankName;
        this.BSRName = BSRName;
        this.ChallanNumber = ChallanNumber;
        this.DateOfDeposit = DateOfDeposit;
        this.AmountOfVoluntaryTaxPayment = AmountOfVoluntaryTaxPayment;
    }
    
    
    public void setBankName(String BankName) {
        this.BankName = BankName;
    }

    public void setBSRName(String BSRName) {
        this.BSRName = BSRName;
    }

    public void setChallanNumber(String ChallanNumber) {
        this.ChallanNumber = ChallanNumber;
    }

    public void setDateOfDeposit(Date DateOfDeposit) {
        this.DateOfDeposit = DateOfDeposit;
    }

    public void setAmountOfVoluntaryTaxPayment(String AmountOfVoluntaryTaxPayment) {
        this.AmountOfVoluntaryTaxPayment = AmountOfVoluntaryTaxPayment;
    }

    public String getBankName() {
        return BankName;
    }

    public String getBSRName() {
        return BSRName;
    }

    public String getChallanNumber() {
        return ChallanNumber;
    }

    public Date getDateOfDeposit() {
        return DateOfDeposit;
    }

    public String getAmountOfVoluntaryTaxPayment() {
        return AmountOfVoluntaryTaxPayment;
    }

}
